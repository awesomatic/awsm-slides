<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       awesomatic.nl
 * @since      0.1
 *
 * @package    Awsm_Slides
 * @subpackage Awsm_Slides/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      0.1
 * @package    Awsm_Slides
 * @subpackage Awsm_Slides/includes
 * @author     Jordi Radstake <jordi@awesomatic.nl>
 */
class Awsm_Slides_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    0.1
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'awsm-slides',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}

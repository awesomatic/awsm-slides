<?php

/**
 * Fired during plugin activation
 *
 * @link       awesomatic.nl
 * @since      0.1
 *
 * @package    Awsm_Slides
 * @subpackage Awsm_Slides/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      0.1
 * @package    Awsm_Slides
 * @subpackage Awsm_Slides/includes
 * @author     Jordi Radstake <jordi@awesomatic.nl>
 */
class Awsm_Slides_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    0.1
	 */
	public static function activate() {

	}

}

<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       awesomatic.nl
 * @since      0.1
 *
 * @package    Awsm_Slides
 * @subpackage Awsm_Slides/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

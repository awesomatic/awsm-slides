<?php

  // Set the labels, this variable is used in the $args array
  $labels = array(
    'name'               => __( 'Slides' ),
    'singular_name'      => __( 'slide' ),
    'add_new'            => __( 'Nieuwe slide' ),
    'add_new_item'       => __( 'Nieuwe slide' ),
    'edit_item'          => __( 'Edit slide' ),
    'new_item'           => __( 'Nieuwe slide', 'awsm-slides' ),
    'all_items'          => __( 'Alle slides', 'awsm-slides' ),
    'view_item'          => __( 'Bekijk slide' ),
    'search_items'       => __( 'Zoek slides' ),
    'featured_image'     => 'Img',
    'set_featured_image' => 'Voeg afbeelding toe'
  );

  // The arguments for our post type, to be entered as parameter 2 of register_post_type()
  $args = array(
    'labels'            => $labels,

    // The only way to read that field is using this code: $obj = get_post_type_object( 'your_post_type_name' ); echo esc_html( $obj->description );
    //'description'       => 'Type some description ..',

    // Whether to exclude posts with this post type from front end search results
    'public'            => true,

    // Check out all available icons: https://developer.wordpress.org/resource/dashicons/#layout
    'menu_icon'         => 'dashicons-text-page',

    // Options: 26 Comments 2 - Dashboard 4 - Seperator 5 - Posts 10 - Media 15 - Links 20 - Pages - 25 Comments - 
    // 59 Seperator - 60 Appaerence - 65 Plugins - 70 Users - 75 Tools - 80 Settings - 99 - Seperator - 200 Seperator - 205 Starter Theme - 210 slides - 300 Seperator
    'menu_position'     => 210,

    // 'comments' editor has to be true to make us of the Gutenberg editor ,'excerpt'
    'supports'          => array( 'title', 'editor', 'thumbnail', 'custom-fields' ),

    'rewrite'            => array( 
        /* 
         * 'slides' Will be used as slug on the permalink 
         */
        'slug'              => 'slides', // Custom string. Just be aware of potential conflicts with other plugins or themes using the same slug.
        /* 
         * Example: if your permalink structure is /blog/, 
         * then your links will be: false->/news/, true->/blog/news/.
         */
        'with_front'        => false // Defaults to |true|
    ),

    //
    //'taxonomies' => array('slide-category'),

    //
    'has_archive'       => false, // Set to false hides Archive Pages

    'publicly_queryable' => false, // Set to false hides Single Pages

    // Has to be true to make use of the the Gutenberg editor
    'show_in_rest'      => true,

    //
    'show_in_admin_bar' => false,

    //
    'show_in_nav_menus' => false,

    //
    'has_archive'       => false,

    // A URL slug for your taxonomy
    'query_var'         => false
  );

  // Call the actual WordPress function
  register_post_type( 'awsm-slide', $args);

  register_taxonomy(
  
    // Give it a name, it will be used as slug as well
    'awsm-slides-category',

    // Connect it to the custom 'slide' type
    'awsm-slide',

    //
    array(
    'label' => __( 'Categories' ),
    'rewrite' => array( 'slug' => 'slide-category' ),
    'hierarchical' => true,
        )

  );

<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              awesomatic.nl
 * @since             0.1
 * @package           Awsm_Slides
 *
 * @wordpress-plugin
 * Plugin Name:       Awesomatic Slides
 * Plugin URI:        awesomatic.nl/plugins/slides
 * Description:       Slider builder for Awesomatic.
 * Version:           0.1
 * Author:            Jordi Radstake
 * Author URI:        awesomatic.nl
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       awsm-slides
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 0.1 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'AWSM_SLIDES_VERSION', '0.1' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-awsm-slides-activator.php
 */
function activate_awsm_slides() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-awsm-slides-activator.php';
	Awsm_Slides_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-awsm-slides-deactivator.php
 */
function deactivate_awsm_slides() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-awsm-slides-deactivator.php';
	Awsm_Slides_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_awsm_slides' );
register_deactivation_hook( __FILE__, 'deactivate_awsm_slides' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-awsm-slides.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    0.1
 */
function run_awsm_slides() {

	$plugin = new Awsm_Slides();
	$plugin->run();

}
run_awsm_slides();
